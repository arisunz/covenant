# covenant

Subnautica-inspired underwater game.

Aggressively WIP. Almost nothing to see yet.

v6 worldgen recommended but v7 works just fine, too.

# Licences

Copyright © 2022 Ariela Wenner

Unless specifically stated otherwise, all code in this repository is licenced under the [MIT license](LICENSE).

All other assets, including textures, models and music, are licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).