minetest.register_alias("mapgen_stone", "nodes:stone")
minetest.register_alias("mapgen_water_source", "nodes:water_source")
minetest.register_alias("mapgen_river_water_source", "nodes:water_source")
minetest.register_alias("mapgen_dirt_with_grass", "nodes:grass")
minetest.register_alias("mapgen_dirt", "nodes:dirt")
minetest.register_alias("mapgen_sand", "nodes:sand")

--
-- Mapgen customization
--

-- commenting this out until I can figure out why does it screw with biome gen
-- minetest.set_mapgen_setting("water_level", covenant.WATER_LEVEL, true)

-- no dungeons
minetest.set_mapgen_setting("mg_flags", "nodungeons", true)

--
-- v5 tweaking
--
minetest.set_mapgen_setting("mgv5_cave_width", 0.02, true)
minetest.set_mapgen_setting("mgv5_cavern_threshold", 0.4, true)
minetest.set_mapgen_setting("mgv5_cavern_limit", -10, true)
minetest.set_mapgen_setting("mgv5_cavern_taper", 50, true)
minetest.set_mapgen_setting("mgv5_large_cave_flooded", 1.0, true)

--
-- v7 tweaking
--
minetest.set_mapgen_setting("mgv7_spflags", "nofloatlands", true)
minetest.set_mapgen_setting("mgv7_cave_width", 0.02, true)
minetest.set_mapgen_setting("mgv7_cavern_threshold", 0.4, true)
minetest.set_mapgen_setting("mgv7_cavern_limit", 10, true)
minetest.set_mapgen_setting("mgv7_cavern_taper", 50, true)
minetest.set_mapgen_setting("mgv7_large_cave_flooded", 1.0, true)

local c_air = minetest.get_content_id("air")
local c_water_source = minetest.get_content_id("nodes:water_source")
local c_flowing_water = minetest.get_content_id("nodes:water_flowing")

-- get rid of air bubbles and "waterfalls" inside caves
minetest.register_on_generated(function(minp, maxp, blockseed)
    if minp.y < covenant.WATER_LEVEL then
        local vm = minetest.get_mapgen_object("voxelmanip")
        local emin, emax = vm:get_emerged_area()
        local data = vm:get_data()
        local area = VoxelArea:new{
            MinEdge = emin,
            MaxEdge = emax
        }
        
        for z = emin.z, emax.z do
            local limit
            if emax.y > covenant.WATER_LEVEL then
                limit = covenant.WATER_LEVEL
            else
                limit = emax.y
            end

            for y = emin.y, limit do
                for x = emin.x, emax.x do
                    local vi = area:index(x, y, z)
                    if (data[vi] == c_air ) or (data[vi] == c_flowing_water) then
                        data[vi] = c_water_source
                    end
                end
            end
        end

        vm:set_data(data)
        vm:write_to_map(true)
    end
end)

minetest.register_on_joinplayer(function(player, last_login)
    player:set_clouds({height = covenant.WATER_LEVEL + 150})
end)

--
-- Biome gen
--

-- YEET ALL THE BIOMES (if any)
minetest.clear_registered_biomes()

minetest.register_biome({
    name = "grassy_surface",
    node_top = "nodes:grass",
    depth_top = 1,

    node_filler = "nodes:dirt",
    depth_filler = 3,

    node_stone = "nodes:stone",

    y_max = 31000,
    y_min = covenant.WATER_LEVEL + 5,

    vertical_blend = 10,

    -- heat_point = 50,
    -- humidity_point = 50,
})

minetest.register_biome({
    name = "shallows",
    node_top = "nodes:sand",
    depth_top = 30,

    node_filler = "nodes:sand",
    depth_filler = 20,

    node_stone = "nodes:dirt",

    y_max = covenant.WATER_LEVEL + 5,
    y_min = covenant.WATER_LEVEL - 50,

    vertical_blend = 10,
    
    -- heat_point = 50,
    -- humidity_point = 50,
})

minetest.register_biome({
    name = "grasslands",

    node_top = "nodes:grass",
    depth_top = 1,

    node_filler = "nodes:dirt",
    depth_filler = 79,

    node_stone = "nodes:stone",

    y_max = covenant.WATER_LEVEL - 40,
    y_min = covenant.WATER_LEVEL - 100,

    vertical_blend = 10,
    
    -- heat_point = 50,
    -- humidity_point = 50,
})

minetest.register_biome({
    name = "ocean_floor",

    node_top = "nodes:dirt",
    depth_top = 5,

    node_filler = "nodes:stone",
    depth_filler = 79,

    node_stone = "nodes:stone",

    y_max = covenant.WATER_LEVEL - 90,
    y_min = covenant.WATER_LEVEL - 120,

    vertical_blend = 10,
    
    -- heat_point = 50,
    -- humidity_point = 50,
})

minetest.register_biome({
    name = "bulb_caves",

    node_top = "nodes:stone",
    depth_top = 5,

    node_filler = "nodes:stone",
    depth_filler = 50,

    node_stone = "nodes:stone",

    y_max = covenant.WATER_LEVEL - 110,
    y_min = -31000,

    vertical_blend = 10,
    
    -- heat_point = 50,
    -- humidity_point = 50,
})

--
-- Decorations
--
minetest.register_decoration({
    deco_type = "simple",
    place_on = {"nodes:grass"},
    bioms = {"grassy_surface"},
    decoration = "nodes:plant_grass_1",
    place_offset_y = -1,
    flags = "force_placement",
})

minetest.register_decoration({
    deco_type = "simple",
    fill_ratio = 0.001,
    place_on = {"nodes:stone"},
    bioms = {"ocean_floor", "bulb_caves"},
    decoration = "nodes:plant_glowhusk",
    place_offset_y = -1,
    flags = "force_placement, all_floors",
})
