minetest.register_node("nodes:cobble", {
    description = "Cobblestone",
	is_ground_content = true,
    tiles = {"cobble.png"},
    drop = 'nodes:cobble',
})

minetest.register_node("nodes:stone", {
	description = "Stone",
	is_ground_content = true,
	tiles = {"stone.png"},
	groups = {cracky = 3, stone = 1},
	drop = 'nodes:cobble',
})

minetest.register_node("nodes:dirt", {
	description = "Dirt",
	is_ground_content = true,
	tiles = {"dirt.png"},
	groups = {crumbly = 3},
	drop = 'nodes:dirt',
})

minetest.register_node("nodes:grass", {
	description = "Dirt with grass",
	is_ground_content = true,
	tiles = {
		"grass_top.png",
		"dirt.png",
		"grass_side.png",
		"grass_side.png",
		"grass_side.png",
		"grass_side.png",
	},
	groups = {crumbly = 3},
	drop = 'nodes:grass',
})

minetest.register_node("nodes:sand", {
	description = "Sand",
	is_ground_content = true,
	tiles = {"sand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	drop = 'nodes:sand',
})

minetest.register_node("nodes:plant_grass_1", {
    drawtype = "plantlike_rooted",
    description = "some grass",
	tiles = {
		"grass_top.png",
		"dirt.png",
		"grass_side.png",
		"grass_side.png",
		"grass_side.png",
		"grass_side.png",
	},
    special_tiles = {{ name = "plant_grass_1.png" }},
    walkable = false,
    waving = 1,
    sunlight_propagates = true,
    paramtype = "light",

    after_destruct  = function(pos, oldnode)
		minetest.set_node(pos, {name = "nodes:grass"})
	end,
})

minetest.register_node("nodes:plant_glowhusk", {
    drawtype = "plantlike_rooted",
    description = "Glowy plant",
	tiles = { "stone.png" },
    special_tiles = {{ name = "plant_glowhusk.png" }},
    walkable = false,
    waving = 1,
    sunlight_propagates = true,
    paramtype = "light",

	is_ground_content = false,

	light_source = 10,

    after_destruct  = function(pos, oldnode)
		minetest.set_node(pos, {name = "nodes:stone"})
	end,
})

minetest.register_node("nodes:water_source", {
	description = "Water Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "water_source.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0,
			},
		},
		{
			name = "water_source.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0,
			},
		},
	},
	use_texture_alpha = "blend",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	waving = 3,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "nodes:water_flowing",
	liquid_alternative_source = "nodes:water_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 100, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, cools_lava = 1},
})

minetest.register_node("nodes:water_flowing", {
	description = "Flowing Water",
	drawtype = "flowingliquid",
	tiles = {"water.png"},
	special_tiles = {
		{
			name = "water_flow.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
		{
			name = "water_flow.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
	},
	use_texture_alpha = "blend",
	paramtype = "light",
	paramtype2 = "flowingliquid",
	sunlight_propagates = true,
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	waving = 3,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_flowing = "nodes:water_flowing",
	liquid_alternative_source = "nodes:water_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 100, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, not_in_creative_inventory = 1,
		cools_lava = 1},
})
