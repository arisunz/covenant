minetest.register_chatcommand("thisbiome", {
  func = function(name)
    local player = minetest.get_player_by_name(name)

    if player ~= nil then
      local pos = player:get_pos()
      local biome = minetest.get_biome_data(pos)
      local name = minetest.get_biome_name(biome.biome)
  
      minetest.log("we are in the "..name)
    end
  end
})
