local biome_skies = {
    -- ["ocean_floor"] = {
    --     base_color = "#56617c",
    --     type = "plain",
    --     clouds = false,
    -- },
    ["bulb_caves"] = {
        base_color = "#313a50",
        type = "plain",
        clouds = false,
    },
}

local players = {}

minetest.register_globalstep(function(dtime)
    for _, player in pairs(minetest.get_connected_players()) do
        local pos = player:get_pos()
        local player_name = player:get_player_name()

        local biome_info = minetest.get_biome_data(pos)
        local biome_name = minetest.get_biome_name(biome_info.biome)
        local biome_sky = biome_skies[biome_name]

        if biome_sky == nil then
            if players[player_name] ~= "default" then
                player:set_sky({type = "regular", clouds = true})
                player:set_sun({visible = true})
                player:set_moon({visible = true})
                player:set_stars({visible = true})
                -- player:set_clouds({height = covenant.WATER_LEVEL + 150})
                players[player_name] = "default"
            end
        else
            if players[player_name] ~= biome_name then
                player:set_sky(biome_sky)
                player:set_sun({visible = false})
                player:set_moon({visible = false})
                player:set_stars({visible = false})
                -- player:set_clouds({density = 0})
                players[player_name] = biome_name
            end
        end
    end
end)